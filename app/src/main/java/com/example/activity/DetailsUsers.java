package com.example.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.db.DatabaseHandler;
import com.example.db.R;
import com.example.db.User;


import org.w3c.dom.Text;


public class DetailsUsers extends Activity implements View.OnClickListener {
    private static String position = null;

   protected void onCreate(Bundle savedInstanceState){
       super.onCreate(savedInstanceState);
       setContentView(R.layout.details_users);
       Intent intent = getIntent();
       position = intent.getStringExtra("position");
       Log.d("Value of position from intent", position);
       DatabaseHandler db = new DatabaseHandler(this);
       User user = db.getUser(position);
       TextView textViewDetailName = (TextView) findViewById(R.id.textViewDetailsName);
       textViewDetailName.setText(user.getName());

       TextView textViewDetailAddress = (TextView) findViewById(R.id.textViewDetailsAddress);
       TextView textViewDetailPhone = (TextView) findViewById(R.id.textViewDetailsPhone);
       textViewDetailPhone.setText(user.getPhone());

       Button backButton = (Button)findViewById(R.id.buttonDetailsBack);
       backButton.setOnClickListener(this);

       Button editButton = (Button)findViewById(R.id.buttonEdit);
       editButton.setOnClickListener(this);

   }


    public void onClick(View v){
        if(v.getId()==R.id.buttonDetailsBack){
            Intent intent = new Intent(getApplicationContext(),ListUsers.class);
            startActivity(intent);

        }else if(v.getId()==R.id.buttonEditDetails){
            Intent intent = new Intent(getApplicationContext(),EditUsers.class);
            intent.putExtra("Position",position);
            startActivity(intent);

        }
    }


}